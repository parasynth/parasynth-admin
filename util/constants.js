export const PROPOSAL_TYPE = {
  ADD_TOKEN: 1,
  ADD_PAIR: 2,
  DELETE_PAIR: 3,
  DELETE_TOKEN: 4,
  1: 'ADD_TOKEN',
  2: 'ADD_PAIR',
  3: 'DELETE_PAIR',
  4: 'DELETE_TOKEN',
};

export const PROPOSAL_ASSET_TYPE = {
  TOKEN: 'token',
  PAIR: 'pair',
}
