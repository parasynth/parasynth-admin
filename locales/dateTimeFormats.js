export default {
  en: {
    short: {
      year: 'numeric', month: 'short', day: 'numeric',
    },
    shortNum: {
      year: 'numeric', month: 'numeric', day: 'numeric',
    },
    monthYear: { month: 'numeric', year: 'numeric' },
    dayMonth: { day: 'numeric', month: 'numeric' },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
    },
    longNum: {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
    },
    time: {
      hour: '2-digit',
      minute: '2-digit',
    },
  },
};
