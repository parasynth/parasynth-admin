import api, { setHTTPClientInstance } from '~/api';

/* eslint-disable */
export default ({ $axios }, inject) => {
  setHTTPClientInstance($axios);
  inject('api', api);
}
