import { createDoaContractInstance, initProviderWallet } from '~/plugins/web3';

export default async ({ store }, inject) => {
  const isAuth = store.getters['auth/isAuth'];

  const init = {
    async initweb3() {
      await initProviderWallet();
    },
    async initDaoContract() {
      await createDoaContractInstance();
    },
    async getUserProfile() {
      await store.dispatch('auth/getUserProfile');
    },
    async initialize() {
      await init.initweb3();
      await init.initDaoContract();
      await init.getUserProfile();
    }
  };

  if (isAuth) {
    await init.initialize();
  }

  inject('init', init);
};
