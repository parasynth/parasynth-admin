/* eslint-disable import/prefer-default-export */
import Web3, { utils } from 'web3';
import BigNumber from 'bignumber.js';
import Web4 from '@cryptonteam/web4';
import config from '~/libs/config';
import abi from '~/libs/abi/index';

let store;
if (process.browser) {
  window.onNuxtReady(({ $store }) => {
    store = $store;
  });
}

BigNumber.config({ EXPONENTIAL_AT: 60 });

let web3Anon;
let web4Anon;
let web3Wallet;
let web4Wallet;

let userAddress;

let daoInstance;

export const output = (res) => ({
  ok: true,
  result: res,
});

export const error = (code, msg, err) => {
  const _err = err ?? new Error(msg);
  _err.ok = false;
  _err.code = code;

  throw _err;
};

export const getWeb3Instances = () => {
  const instances = {
    web3Anon,
    web4Anon,
    web3Wallet,
    web4Wallet,
  };

  return instances;
};

export const createContractInstace = async (contractAbi, address) => {
  const contractAbstraction = web4Wallet.getContractAbstraction(contractAbi);
  const contractInstance = await contractAbstraction.getInstance(address);

  return contractInstance;
};

export const createDoaContractInstance = async () => {
  daoInstance = await createContractInstace(abi.dao, config.dao);
  console.log('daoInstance: ', daoInstance);
};

export const addProposal = async (description, bytecode) => {
  console.log('config: ', config);
  console.log('bytecode: ', bytecode);
  console.log('description: ', description);
  console.log('config.swap: ', config.swap);
  let proposal = null;
  try {
    proposal = await daoInstance.addProposal(config.swap, description, bytecode);
    console.log('ADD proposal: ', proposal);
  } catch (err) {
    console.log('ADD PROPOSAL ERR: ');
    console.dir(err);
    return error(500, err.message, err)
  }
  return proposal;
};

export const initProviderWallet = async (address) => {
  console.log('initProviderWallet', address);
  try {
    await window.ethereum.enable();
    web3Wallet = new Web3(window.ethereum);

    let userNetwork;

    await Promise.all([
      userNetwork = (await web3Wallet.eth.net.getId()).toString(),
      userAddress = await web3Wallet.eth.getCoinbase(),
    ]);

    console.log('initProvider', typeof userNetwork, userNetwork, typeof config.chainId, config.chainId);
    if (userNetwork !== config.chainId.toString()) {
      return error(433, 'incorrect network');
    }

    web4Wallet = new Web4();
    await web4Wallet.setProvider(window.ethereum, userAddress);

    return output({
      address: userAddress,
    });
  } catch (err) {
    if (err.code === 433) throw err;

    return error(500, 'connection error', err);
  }
};

export const initWeb3 = async () => {
  try {
    let infuraUrl;
    if (+config.chainId === 1) {
      infuraUrl = `wss://mainnet.infura.io/ws/v3/${config.infura}`;
    } else {
      infuraUrl = `wss://rinkeby.infura.io/ws/v3/${config.infura}`;
    }
    const providerInfura = new Web3.providers.WebsocketProvider(infuraUrl);
    web4Anon = new Web4();
    web3Anon = new Web3(new Web3.providers.WebsocketProvider(infuraUrl));
    console.log('web3Anon: ', web3Anon);
    web4Anon.setProvider(providerInfura);

    return output({ web3Anon, web4Anon });
  } catch (err) {
    if (err.code === 433) return err;

    return error(500, 'connection error', err);
  }
};
