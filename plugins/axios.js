/* eslint-disable */
export default function (ctx) {
  const {
    $axios, store, redirect,
  } = ctx;

  $axios.onRequest((config) => {
    if (store.getters['auth/isAuth']) {
      if (config.url.split('/').pop() === 'refresh') {
        config.headers.authorization = `Bearer ${store.getters['auth/refreshToken']}`;
      } else {
        config.headers.authorization = `Bearer ${store.getters['auth/accessToken']}`;
      }
    }
  });

  $axios.onError(async (error) => {
    const originalRequest = error.config;

    if (window.localStorage.getItem('refresh')) {
      if ((error.response.status == 401 || error.response.status == 403) && originalRequest.url.split('/').pop() === 'refresh') {
        store.dispatch('auth/logout');
        redirect('/login');
        throw error;
      }

      if ((error.response.status == 401 || error.response.status == 403) && !originalRequest._retry) {
        originalRequest._retry = true;

        try {
          await store.dispatch(`auth/refreshTokens`);
          return $axios(originalRequest);
        } catch (err) {
          console.log('originalRequest err: ', err);
        }
      }
    }
    throw error;
  });

  const access = window.localStorage.getItem('access');
  const refresh = window.localStorage.getItem('refresh');

  if (access && refresh) {
    store.commit('auth/setOldTokens', { access, refresh });
  }
}
