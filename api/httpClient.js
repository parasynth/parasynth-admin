let axios;

const setHTTPClientInstance = (instance) => {
  axios = instance;
};

const $get = (url, config = null) => axios.$get(url, config);

const $post = (url, data = null, config = null) => axios.$post(url, data, config);

const $delete = (url, config = null) => axios.$delete(url, config);

const $put = (url, data = null, config = null) => axios.$put(url, data, config);

export default {
  $get,
  $post,
  $delete,
  $put,
};

export {
  setHTTPClientInstance,
};
