import httpClient from '../httpClient';

const getTemporaryPairs = () => httpClient.$get('/admin/pair/')
  .then((res) => {
    const { pairs } = res.result;
    return pairs;
  });

const getPairs = () => httpClient.$get('/pair/').then(res => res.result.pairs);
const editPairById = (id, payload) => httpClient.$post(`/admin/change/pair/${id}`, payload).then(res => res.result);
const createPair = (payload) => httpClient.$post('/admin/create/pair/', payload);

export default {
  getTemporaryPairs,
  getPairs,
  editPairById,
  createPair,
};
