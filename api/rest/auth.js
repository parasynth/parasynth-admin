import httpClient from '../httpClient';

const login = async (credentials) => {
  const res = await httpClient.$post('/auth/login', credentials);

  return res.result;
};

const refreshTokens = async () => {
  const tokens = await httpClient.$get('/auth/refresh');
  return tokens.result;
};

const getUserProfile = async () => httpClient.$get('/admin/profile/').then((res) => res.result.profile);

export default {
  login,
  refreshTokens,
  getUserProfile,
};
