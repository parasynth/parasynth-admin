import httpClient from '../httpClient';

const getCommissions = () => httpClient.$get('/admin/commission/');

const editCommissions = (commissions) => httpClient.$post('/admin/change/commission/', commissions);

export default {
  getCommissions,
  editCommissions
};
