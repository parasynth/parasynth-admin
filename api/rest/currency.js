import httpClient from '../httpClient';

const getTemporaryCurrencies = () => httpClient.$get('/admin/currency/')
  .then((res) => {
    const { coins } = res.result;

    return coins;
  });

const getCurrencies = () => httpClient.$get('/currency/').then(res => res.result.coin);
const createCurrency = (payload) => httpClient.$post('/admin/create/currency/', payload).then(res => res.result);
const editCurrencyById = (id, payload) => httpClient.$post(`/admin/change/currency/${id}/`, payload)
  .then((res) => res.result);

export default {
  getCurrencies,
  getTemporaryCurrencies,
  createCurrency,
  editCurrencyById,
};
