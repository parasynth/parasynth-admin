import httpClient from '../httpClient';

const getProposals = () => httpClient.$get('/admin/temporary/proposal/')
  .then((res) => res.result.proposals);

const getProposalById = (id) => httpClient.$get(`/admin/temporary/proposal/${id}/`)
  .then((res) => res.result.proposals);

export default {
  getProposals,
  getProposalById,
};
