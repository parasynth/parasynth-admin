import { setHTTPClientInstance } from './httpClient';
import commissionsService from './rest/commissions';
import authService from './rest/auth';
import pairService from './rest/pair';
import currencyService from './rest/currency';
import proposalService from './rest/proposal';

export default {
  commissionsService,
  authService,
  pairService,
  currencyService,
  proposalService,
};

export {
  setHTTPClientInstance,
};
