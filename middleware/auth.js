export default ({ store, redirect }) => {
  if (!store.getters['auth/isAuth']) {
    return redirect('/login');
  }
  return true;
};
