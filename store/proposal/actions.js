import { addProposal } from "~/plugins/web3";

export default {
  async getProposals({ commit }) {
    const proposals = await this.$api.proposalService.getProposals();
    console.log('proposals: ', proposals);

    commit('setProposals', proposals);
  },
  async createProposal({ commit }, proposalId) {
    const proposalInfo = await this.$api.proposalService.getProposalById(proposalId);
    console.log('proposalInfo: ', proposalInfo);
    const proposalDao = await addProposal(proposalInfo.data.symbolBase, proposalInfo.bytecode);
    console.log('addProposal: ', proposalDao);

    return proposalDao;
  },
  async subscribeToProposals({ commit }) {
    await this.$ws.subscribe('/admin/temporary/proposal/', (message) => {
      commit('updateProposals', message);
    })
  },
  async unsubscribeToProposals({ commit }) {
    await this.$ws.unsubscribe('/admin/temporary/proposal/');
  }
};
