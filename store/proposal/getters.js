export default {
  getProposals: (state) => state.proposals.data,
  getProposalsCount: (state) => state.proposals.count,
};
