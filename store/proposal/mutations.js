import { PROPOSAL_TYPE } from '~/util/constants';
import { PROPOSAL_ASSET_TYPE } from '../../util/constants';

export default {
  setProposals(state, proposals) {
    const preapredProposal = proposals.map((proposal) => {
      if (proposal.type === PROPOSAL_TYPE.ADD_PAIR || proposal.type === PROPOSAL_TYPE.DELETE_PAIR) {
        proposal.symbol = `${proposal.data.symbolBase}/${proposal.data.symbolQuote}`;
        proposal.assetType = PROPOSAL_ASSET_TYPE.PAIR;

        return proposal;
      }

      if (proposal.type ===  PROPOSAL_TYPE.ADD_TOKEN || proposal.type === PROPOSAL_TYPE.DELETE_TOKEN) {
        proposal.symbol = `${proposal.data.symbolBase}`;
        proposal.assetType = PROPOSAL_ASSET_TYPE.TOKEN;

        return proposal;
      }
    });
    state.proposals.count = preapredProposal.length;
    state.proposals.data = preapredProposal;
  },
  updateProposals(state, proposal) {
    state.proposals.data = state.proposals.data.filter((i) => i.id !== proposal.id);
    state.proposals.count -= 1;
  }
};
