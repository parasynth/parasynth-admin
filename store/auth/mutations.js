export default {
  setTokens(state, auth) {
    state.tokens.access = auth.access;
    state.tokens.refresh = auth.refresh;
    state.status = auth.status;
    window.localStorage.setItem('access', auth.access);
    window.localStorage.setItem('refresh', auth.refresh);
  },
  logout(state) {
    window.localStorage.removeItem('access');
    window.localStorage.removeItem('refresh');
    state.tokens.access = '';
    state.tokens.refresh = '';
    state.status = '';
  },
  setOldTokens(state, tokens) {
    state.tokens.access = tokens.access;
    state.tokens.refresh = tokens.refresh;
  },
  updateTokens(state, tokens) {
    state.tokens.access = tokens.access;
    state.tokens.refresh = tokens.refresh;
    window.localStorage.setItem('access', tokens.access);
    window.localStorage.setItem('refresh', tokens.refresh);
  },
  setUserProfile(state, user) {
    state.user = user;
  },
};
