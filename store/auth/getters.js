export default {
  isAuth: ({ tokens }) => Boolean(tokens.access && tokens.refresh),
  accessToken: (state) => state.tokens.access,
  refreshToken: (state) => state.tokens.refresh,
  getUserProfile: (state) => state.user,
};
