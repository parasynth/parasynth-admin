export default {
  async login({ commit }, credentials) {
    const auth = await this.$api.authService.login(credentials);

    commit('setTokens', auth);
  },
  logout({ commit }) {
    commit('logout');
  },
  async refreshTokens({ commit }) {
    const tokens = await this.$api.authService.refreshTokens();

    commit('updateTokens', tokens);
  },
  async getUserProfile({ commit }) {
    const user = await this.$api.authService.getUserProfile();
    commit('setUserProfile', user);
  },
  async initialize() {
    await this.$init.initialize();
  }
};
