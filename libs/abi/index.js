import dao from './dao';
import erc20 from './erc20';

export default {
  dao,
  erc20
}
