const infura = process.env.INFURA;
const chainId = process.env.CHAIN_ID;
const dao = process.env.DAO_CONTRACT_ADDRESS;
const swap = process.env.SWAP_CONTRACT_ADDRESS;

const config = {
  infura,
  chainId,
  dao,
  swap,
};
export default config;
